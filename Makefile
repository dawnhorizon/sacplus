CC = gcc
CFLAGS = -std=c99 -g -W 

# OBJS
PROJ_DIR = .
INCLUDE_DIR = ${PROJ_DIR}/include

TRACES_DIR = ${PROJ_DIR}/traces
LIB_DIR = ${PROJ_DIR}/lib
OBJ_LIBS = ${LIB_DIR}/libzone.o 

UTIL_DIR = ${PROJ_DIR}/util
OBJ_UTILS =	${UTIL_DIR}/bitmap.o \
			${UTIL_DIR}/hashtable.o \
			${UTIL_DIR}/xstrtoumax.o

SRC_DIR = ${PROJ_DIR}/src
DM_SIMU_DIR = ${PROJ_DIR}/smr-simulator-alpha
OBJ_DM_SIMULATOR = ${DM_SIMU_DIR}/simulator_alpha.o ${DM_SIMU_DIR}/hashtb_pb.o


OBJ_ALGORITHM = ${SRC_DIR}/zbd-cache.o

STRATEGY_DIR = ${SRC_DIR}/strategy
OBJ_STRATEGIES = ${STRATEGY_DIR}/cars.o \
				 ${STRATEGY_DIR}/most.o \
				 ${STRATEGY_DIR}/most_cmrw.o \
				 ${STRATEGY_DIR}/lru_zone.o \
				 ${STRATEGY_DIR}/cars_cacheprop.o \
				 ${STRATEGY_DIR}/pore.o \
				 ${STRATEGY_DIR}/smrc_ieeeaccess.o \
				 ${STRATEGY_DIR}/sla_ica3pp.o \
				 ${STRATEGY_DIR}/crea.o \
				 ${STRATEGY_DIR}/offline/ei_greedy.o \
				 ${STRATEGY_DIR}/offline/ei_greedy_N.o \
				 ${STRATEGY_DIR}/offline/maxminRI_greedy.o \
				 ${STRATEGY_DIR}/offline/sumRI.o \
				 ${STRATEGY_DIR}/offline/meanRI.o \
				 ${STRATEGY_DIR}/offline/min.o \
				 ${STRATEGY_DIR}/offline/eiri.o \
				 ${STRATEGY_DIR}/offline/zoneopt.o
TOOLS_DIR = ${PROJ_DIR}/tools
OBJ_TOOLS = ${TOOLS_DIR}/zbd_set_full \
			${TOOLS_DIR}/zbd_set_empty

# DLL
DLL = zbc 
MATHDDL = m

CFLAGS += -I${PROJ_DIR} -I$(INCLUDE_DIR) -I$(LIB_DIR) -I$(UTIL_DIR) -I$(SRC_DIR) -I$(STRATEGY_DIR) -I${DM_SIMU_DIR} $(MACROS)


default: build-libs build-utils build-algorithm build-tools build-dm-simulator test

test: 
	$(CC) $(CFLAGS) -l$(DLL) -l$(MATHDDL) $(OBJ_LIBS) $(OBJ_UTILS) $(OBJ_ALGORITHM) $(OBJ_STRATEGIES) ${OBJ_DM_SIMULATOR} main.c -o test

build-libs:
	$(CC) $(CFLAGS) -c ${LIB_DIR}/libzone.c -o ${LIB_DIR}/libzone.o

build-utils:
	$(CC) $(CFLAGS) -c ${UTIL_DIR}/bitmap.c -o ${UTIL_DIR}/bitmap.o
	$(CC) $(CFLAGS) -c ${UTIL_DIR}/hashtable.c -o ${UTIL_DIR}/hashtable.o
	$(CC) $(CPPFLAGS) $(CFLAGS) -c ${UTIL_DIR}/xstrtoumax.c -o ${UTIL_DIR}/xstrtoumax.o

build-algorithm:
	$(CC) $(CFLAGS) -c ${SRC_DIR}/zbd-cache.c -o ${SRC_DIR}/zbd-cache.o
	$(CC) $(CFLAGS) -c ${STRATEGY_DIR}/cars.c -o ${STRATEGY_DIR}/cars.o
	$(CC) $(CFLAGS) -c ${STRATEGY_DIR}/most.c -o ${STRATEGY_DIR}/most.o
	$(CC) $(CFLAGS) -c ${STRATEGY_DIR}/most_cmrw.c -o ${STRATEGY_DIR}/most_cmrw.o
	$(CC) $(CFLAGS) -c ${STRATEGY_DIR}/lru_zone.c -o ${STRATEGY_DIR}/lru_zone.o
	$(CC) $(CFLAGS) -c ${STRATEGY_DIR}/cars_cacheprop.c -o ${STRATEGY_DIR}/cars_cacheprop.o
	$(CC) $(CFLAGS) -c ${STRATEGY_DIR}/pore.c -o ${STRATEGY_DIR}/pore.o
	$(CC) $(CFLAGS) -c ${STRATEGY_DIR}/smrc_ieeeaccess.c -o ${STRATEGY_DIR}/smrc_ieeeaccess.o
	$(CC) $(CFLAGS) -c ${STRATEGY_DIR}/sla_ica3pp.c -o ${STRATEGY_DIR}/sla_ica3pp.o
	$(CC) $(CFLAGS) -c ${STRATEGY_DIR}/crea.c -o ${STRATEGY_DIR}/crea.o
	$(CC) $(CFLAGS) -c ${STRATEGY_DIR}/offline/ei_greedy.c -o ${STRATEGY_DIR}/offline/ei_greedy.o
	$(CC) $(CFLAGS) -c ${STRATEGY_DIR}/offline/ei_greedy_N.c -o ${STRATEGY_DIR}/offline/ei_greedy_N.o
	$(CC) $(CFLAGS) -c ${STRATEGY_DIR}/offline/maxminRI_greedy.c -o ${STRATEGY_DIR}/offline/maxminRI_greedy.o
	$(CC) $(CFLAGS) -c ${STRATEGY_DIR}/offline/sumRI.c -o ${STRATEGY_DIR}/offline/sumRI.o
	$(CC) $(CFLAGS) -c ${STRATEGY_DIR}/offline/meanRI.c -o ${STRATEGY_DIR}/offline/meanRI.o
	$(CC) $(CFLAGS) -c ${STRATEGY_DIR}/offline/min.c -o ${STRATEGY_DIR}/offline/min.o
	$(CC) $(CFLAGS) -c ${STRATEGY_DIR}/offline/eiri.c -o ${STRATEGY_DIR}/offline/eiri.o
	$(CC) $(CFLAGS) -c ${STRATEGY_DIR}/offline/zoneopt.c -o ${STRATEGY_DIR}/offline/zoneopt.o
	
build-dm-simulator:
	$(CC) $(CFLAGS) -c ${DM_SIMU_DIR}/simulator_alpha.c -o ${DM_SIMU_DIR}/simulator_alpha.o
	$(CC) $(CFLAGS) -c ${DM_SIMU_DIR}/hashtb_pb.c -o ${DM_SIMU_DIR}/hashtb_pb.o

build-tools:
	$(CC) $(CFLAGS) -l$(DLL) ${TOOLS_DIR}/zbd_set_full.c -o ${TOOLS_DIR}/zbd_set_full.out
	$(CC) $(CFLAGS) -l$(DLL) ${TOOLS_DIR}/zbd_set_empty.c -o ${TOOLS_DIR}/zbd_set_empty.out

clean: 
	rm -f ./*.o

	rm -f ${LIB_DIR}/*.o
	rm -f ${UTIL_DIR}/*.o
	rm -f ${ALGORITHM}/*.o
	rm -f ${SRC_DIR}/*.o
	rm -f ${STRATEGY_DIR}/*.o
	rm -f ${STRATEGY_DIR}/offline/*.o
	rm -f ${OBJ_TOOLS}

	rm -rf ${TRACES_DIR}/tracefilehashmap*
	rm -f ./test
