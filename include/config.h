#ifndef _CONFIG_H
#define _CONFIG_H

#include <stdint.h>

static const char *TRACE_FILES[] = {
    // "./traces/src1_2.csv.req",
    // "./traces/wdev_0.csv.req",
    // "./traces/hm_0.csv.req",
    // "./traces/mds_0.csv.req",
    // "./traces/prn_0.csv.req",
    // "./traces/rsrch_0.csv.req",
    "./traces/trace/LUN0.txt",
    "./traces/trace/LUN1.txt",
    "./traces/trace/LUN2.txt",
    "./traces/trace/LUN3.txt",
    "./traces/trace/LUN4.txt",
    "./traces/trace/LUN6.txt",
    "./traces/stg_0.csv.req",
    "./traces/ts_0.csv.req",
    "./traces/usr_0.csv.req",
    "./traces/web_0.csv.req",
   // "./traces/production-LiveMap-Backend-4K.req", // --> not in used.
    "/home/mnt/home/fei/devel/zbd/sac4hm/traces/long.csv.req.full",                       // default set: cache size = 8M*blksize; persistent buffer size = 1.6M*blksize.
    "./traces/src1_2.csv.req"
};

/* GLOBAL */
#ifndef ZBD_GENERAL
#define ZBD_GENERAL
#   define SECSIZE 512
#   define BLKSIZE 4096
#   define N_BLKSEC 8
#endif

#ifndef ZBD_SPEC
#define ZBD_SPEC5

// DM-SMR
// #   define ZONESIZE  33554432
// #   define N_ZONESEC 65536
// #   define N_ZONEBLK 8192

// #   define N_ZONES 218750    // = N_COV_ZONE + N_SEQ_ZONES
// #   define N_COV_ZONE 0
// #   define N_SEQ_ZONES 218750

//HM-SMR
#   define ZONESIZE  268435456
#   define N_ZONESEC 524288
#   define N_ZONEBLK 65536

#   define N_ZONES 37256    // = N_COV_ZONE + N_SEQ_ZONES
#   define N_COV_ZONE 378
#   define N_SEQ_ZONES 36878

#endif

#define NO_REAL_DISK_IO 

//#define DEBUG
#ifdef DEBUG
# define DEBUG_CARS
#endif

/* TEST DEVICE */
static char config_dev_zbd[] = "/mnt/sacplus/smr"; //"/mnt/smr/dm";
static char config_dev_cache[] = "/mnt/sacplus/cache"; //"/mnt/ssd/ssd"; //"/de:qv/nvme0n1"; //"/dev/nvme0n1";// ; //; //"/mnt/cache/raw"; 
static char config_trace_seq_mmap_file_prefix[] = "./traces/tracefilemmp";
static char config_trace_hashmap_dir_prefix[] = "./traces/tracefilehashmap";

//#define ZBD_DRIVE_EMU
#ifdef ZBD_DRIVE_EMU
#   define ZBD_OFLAG ZBC_O_DRV_FAKE
#else
#   define ZBD_OFLAG 0
#endif


/* ALOGORITHM RELATED */

/* Trace Type */
#define TRACE_SYSTOR17

#ifdef TRACE_SYSTOR17
    #define ACT_READ 'R'
    #define ACT_WRITE 'W'
#else
    #define ACT_READ 0x00
    #define ACT_WRITE 0x01
#endif

#endif
