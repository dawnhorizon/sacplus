
#!/bin/bash

DATETIME="`date +%Y-%m-%d,%H:%m:%s`"  

cd ..

type ./test > /dev/null 2>&1

CMD_TEST=$?

if [ $CMD_TEST -ne 0 ]; then 
	echo "error: No executable program file <sac>, please check if the program has been built."
	exit 1
fi

mkdir ./log/${DATETIME}

echo "Testing.. CARS WO 16G"
 ./test --algorithm CARS  --cache-size 64G --rmw-part 0 --workload 10 --workload-mode w --requests 2000000000 --smr-type DM > ./log/${DATETIME}/log_sac_wo_64g_hm_r187m.log &
# ./test --algorithm SLA  --cache-size 64G --rmw-part 0 --workload 10 --workload-mode w --requests 2000000000 --smr-type DM > ./log/${DATETIME}/log_sla_wo_64g_hm_r187m.log & 
# ./test --algorithm MOST  --cache-size 64G --rmw-part 0 --workload 10 --workload-mode w --requests 2000000000 --smr-type DM > ./log/${DATETIME}/log_most_wo_64g_hm_r187m.log &
# ./test --algorithm SMRC  --cache-size 64G --rmw-part 0 --workload 10 --workload-mode w --requests 2000000000 --smr-type DM > ./log/${DATETIME}/log_smrc_wo_64g_hm_r187m.log &

 ./test --algorithm CARS  --cache-size 96G --rmw-part 0 --workload 10 --workload-mode w --requests 2000000000 --smr-type DM > ./log/${DATETIME}/log_sac_wo_96g_hm_r187m.log &
# ./test --algorithm SLA  --cache-size 96G --rmw-part 0 --workload 10 --workload-mode w --requests 2000000000 --smr-type DM > ./log/${DATETIME}/log_sla_wo_96g_hm_r187m.log & 
# ./test --algorithm MOST  --cache-size 96G --rmw-part 0 --workload 10 --workload-mode w --requests 2000000000 --smr-type DM > ./log/${DATETIME}/log_most_wo_96g_hm_r187m.log &
# ./test --algorithm SMRC  --cache-size 96G --rmw-part 0 --workload 10 --workload-mode w --requests 2000000000 --smr-type DM > ./log/${DATETIME}/log_smrc_wo_96g_hm_r187m.log &

echo "Done. "





