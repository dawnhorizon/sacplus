#ifndef SMR_SSD_CACHE_SMR_EMULATION_H
#define SMR_SSD_CACHE_SMR_EMULATION_H

#include "hashtb_pb.h"

#define DEBUG 0
/* ---------------------------smr simulator---------------------------- */

#define NBLOCK_SMR_PB 150000 // 600MB persistent buffer size
typedef struct
{
    DespTag tag;
    long    despId;
    int     isValid;
} FIFODesc;

typedef struct
{
	unsigned long	n_used;
             long   head, tail;
} FIFOCtrl;

extern int  fd_fifo_part;
extern int  fd_smr_part;
extern int InitDMSimulator();
extern int simu_dmsmr_read(char *buffer, size_t size, uint64_t offset);
extern int simu_dmsmr_write(char *buffer, size_t size, uint64_t offset);
extern void DM_Simulator_PrintStatistic();
#endif
