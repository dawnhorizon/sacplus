#ifndef CREA_H
#define CREA_H

#include <stdint.h>
#include "config.h"

struct cache_page;


extern int crea_init();
extern int crea_login(struct cache_page *page, int op);
extern int crea_hit(struct cache_page *page, int op);
extern int crea_logout(struct cache_page *page, int op);

extern int crea_writeback_privi(int type);

extern int crea_flush_allcache();

#endif