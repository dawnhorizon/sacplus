#ifndef EIGREEDY_H
#define EIGREEDY_H

#include <stdint.h>


struct cache_page;


extern int eigreedy_init();
extern int eigreedy_login(struct cache_page *page, int op);
extern int eigreedy_hit(struct cache_page *page, int op);
extern int eigreedy_logout(struct cache_page *page, int op);

extern int eigreedy_writeback_privi(int type);

#endif //EIGREEDY