#ifndef EIGREEDY_N_H
#define EIGREEDY_N_H

#include <stdint.h>


struct cache_page;


extern int eigreedyN_init();
extern int eigreedyN_login(struct cache_page *page, int op);
extern int eigreedyN_hit(struct cache_page *page, int op);
extern int eigreedyN_logout(struct cache_page *page, int op);

extern int eigreedyN_writeback_privi(int type);

#endif //EIGREEDY_N_H