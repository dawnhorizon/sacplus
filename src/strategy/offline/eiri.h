#ifndef EIRI_H
#define EIRI_H

#include <stdint.h>


struct cache_page;


extern int eiri_init();
extern int eiri_login(struct cache_page *page, int op);
extern int eiri_hit(struct cache_page *page, int op);
extern int eiri_logout(struct cache_page *page, int op);

extern int eiri_writeback_privi(int type);

#endif //EIRI_H