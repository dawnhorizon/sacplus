#ifndef maxminRI_greedy_H
#define maxminRI_greedy_H

#include <stdint.h>


struct cache_page;


extern int maxminRI_greedy_init();
extern int maxminRI_greedy_login(struct cache_page *page, int op);
extern int maxminRI_greedy_hit(struct cache_page *page, int op);
extern int maxminRI_greedy_logout(struct cache_page *page, int op);

extern int maxminRI_greedy_writeback_privi(int type);

#endif //EIGREEDY