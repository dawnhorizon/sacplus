#ifndef meanRI_H
#define meanRI_H

#include <stdint.h>


struct cache_page;


extern int meanRI_init();
extern int meanRI_login(struct cache_page *page, int op);
extern int meanRI_hit(struct cache_page *page, int op);
extern int meanRI_logout(struct cache_page *page, int op);

extern int meanRI_writeback_privi(int type);

#endif //EIGREEDY