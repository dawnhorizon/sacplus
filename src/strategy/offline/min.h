#ifndef min_H
#define min_H

#include <stdint.h>


struct cache_page;


extern int min_init();
extern int min_login(struct cache_page *page, int op);
extern int min_hit(struct cache_page *page, int op);
extern int min_logout(struct cache_page *page, int op);

extern int min_writeback_privi(int type);

#endif //EIGREEDY