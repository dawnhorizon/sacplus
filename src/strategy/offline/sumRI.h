#ifndef sumRI_H
#define sumRI_H

#include <stdint.h>


struct cache_page;


extern int sumRI_init();
extern int sumRI_login(struct cache_page *page, int op);
extern int sumRI_hit(struct cache_page *page, int op);
extern int sumRI_logout(struct cache_page *page, int op);

extern int sumRI_writeback_privi(int type);

#endif //EIGREEDY