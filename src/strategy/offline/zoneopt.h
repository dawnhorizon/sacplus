#ifndef zoneopt_H
#define zoneopt_H

#include <stdint.h>


struct cache_page;


extern int zoneopt_init();
extern int zoneopt_login(struct cache_page *page, int op);
extern int zoneopt_hit(struct cache_page *page, int op);
extern int zoneopt_logout(struct cache_page *page, int op);

extern int zoneopt_writeback_privi(int type);

#endif //EIGREEDY