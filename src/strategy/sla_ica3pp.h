#ifndef SLA_ICA3PP_H
#define SLA_ICA3PP_H

#include <stdint.h>

struct cache_page;


extern int sla_ica3pp_init();
extern int sla_ica3pp_login(struct cache_page *page, int op);
extern int sla_ica3pp_hit(struct cache_page *page, int op);
extern int sla_ica3pp_logout(struct cache_page *page, int op);

extern int sla_ica3pp_writeback_privi(int type);

#endif