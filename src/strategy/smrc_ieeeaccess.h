#ifndef SMRC_IEEEACESS_H
#define SMRC_IEEEACESS_H

#include <stdint.h>


struct cache_page;


extern int smrc_ieeeaccess_init();
extern int smrc_ieeeaccess_login(struct cache_page *page, int op);
extern int smrc_ieeeaccess_hit(struct cache_page *page, int op);
extern int smrc_ieeeaccess_logout(struct cache_page *page, int op);

extern int smrc_ieeeaccess_writeback_privi(int type);


#endif